<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Candidato;

class CandidatoController extends Controller {

    /**
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
      $candidato = new Candidato([
        'nome' => $request->get('nome'),
        'email'=> $request->get('email'),
        'nascimento'=> $request->get('nascimento'),
        'telefone'=> $request->get('telefone')
      ]);

      $candidato = $candidato->save();

      return response()->json($candidato);
    }

    /**
     *
     * @return \Illuminate\Http\Response
     */
    public function listAll(Request $request) {
        $candidatos = Candidato::orderBy('id','desc')->take(4)->get();

        return response()->json($candidatos);
      }

}
