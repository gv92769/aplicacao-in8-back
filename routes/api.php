<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(array('prefix' => 'in8'), function() {

  Route::post('candidato/salvar', 'CandidatoController@store');
  Route::get('candidato/listAll', 'CandidatoController@listAll');

});
