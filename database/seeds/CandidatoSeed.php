<?php

use Illuminate\Database\Seeder;
use App\Models\Candidato;

class CandidatoSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $candidato = new Candidato([
            'nome' => 'Gabriel Vinicius dos Santos',
            'email' => 'gv92769@gmail.com',
            'nascimento' => '1997-10-02',
            'telefone' => '(31) 9 9362-4987'
        ]);

        $candidato->save();

        $candidato = new Candidato([
            'nome' => 'Fulano Beltrano de Oliveira da Silva',
            'email' => 'fulanobos@gmail.com',
            'nascimento' => '1997-10-02',
            'telefone' => '(31) 9 9666-1111'
        ]);

        $candidato->save();
    }
}
